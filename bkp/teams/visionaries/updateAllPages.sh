#!/bin/bash

cd ..

# ANCHOR: Backup
echo "Backing up teams..."
cd ..
mkdir bkp
cp -r teams bkp/
cd teams

echo "Updating teams..."
for d in */ ; do
    echo "Updating $d"
    if [ "$d" = "template/" ]; then
        continue
    fi
    cp template/script.js $d/
    cp template/index.html $d/
done

echo "DONE!"
