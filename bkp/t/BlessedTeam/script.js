// var appStoreLink =
//   team === "XTremeNation"
//     ? "https://apps.apple.com/us/app/xtreme-nation/id1360107458"
//     : null;
// team, googlePlaystoreLink in unique.js
// var debug = team === "TEAM_NAME" comment
var debug = false;
var code;
var fetchAttempted;
var fetchFailed;

if (debug === true) testFlightLink = "https://apptakeoff.com";

var device = deviceType(); // "ios", "android", "other"
// device = "ios"; // debug
console.log("real device = " + device);
var deviceOverride = getURLVaraible("device");
if (deviceOverride && deviceOverride.length > 0) device = deviceOverride;
console.log("set device = " + device);

var overrideIOSVersion = getURLVaraible("ios");
var iosVersion =
  device === "ios"
    ? overrideIOSVersion && overrideIOSVersion.toString().length > 0
      ? overrideIOSVersion
      : getIOSVersion().Full_Release_Numeric
    : 0;
console.log("ios version: " + iosVersion);

var serveTestFlight;
var _testFlightOverride = getURLVaraible("testflight");
if (_testFlightOverride === "true" || (testFlightLink && !homebase)) {
  console.log(
    "serving test flight link regardless of other configurations due to url variable"
  );
  if (device === "ios") serveTestFlight = true;
}

var country = "US";
getCountry().then((_country) => {
  if (_country && _country.length > 0) country = _country;

  // country url bar override (for testing purposes)
  var _countryOverride = getURLVaraible("country");
  if (_countryOverride && _countryOverride.length > 0) {
    country = _countryOverride;
    console.log("country overriden from url and set to " + country);
  }

  console.log("homebase is " + homebase + " and country is " + country);
  if (homebase && country !== homebase && device === "ios") {
    console.log("homebase and country are different...");
    if (getURLVaraible("admin") == "true") {
      console.log("TestFlight serve cancelled due to admin.");
      return;
    }
    console.log(
      `iPhone user is from ${country} but homebase is ${homebase}... Serving TestFlight link...`
    );
    if (testFlightLink && device === "ios") {
      serveTestFlight = true;
      onLoad();
      // onLoad({
      //   serveTestFlight: true,
      // });
    } else {
      console.log(
        "Attempted to serve TestFlight to iPhone user, but it is not set in the unique.js"
      );
    }
  }
});

var shouldUpdateApp = getURLVaraible("update") == 1 ? true : false;
if (shouldUpdateApp === true) setupForAppUpdate();
console.log("shouldUpdateApp: " + shouldUpdateApp);

function hideAllBlocks() {
  document.getElementById("generate-block").style.display = "none";
  document.getElementById("install-block").style.display = "none";
  document.getElementById("loading-block").style.display = "none";
}

function onLoad(options = {}) {
  // const { serveTestFlight } = options;
  // when page loads
  // var deviceOverride = getURLVaraible("device");
  // if (deviceOverride && deviceOverride.length > 0) device = deviceOverride;
  // console.log("device = " + deviceType);
  if (device == "other" && !debug) {
    // ANCHOR: Web
    wrongDevice();
  } else if (device == "android") {
    // ANCHOR: Android
    hideAllBlocks();
    document.getElementById("ios-abm-howto").style.display = "none";
    document.getElementById("ios-howto-header").style.display = "none";
    document.getElementById("install-block").style.display = "block";
    document.getElementById("app-img-install").src =
      "../../assets/playstore.svg";
    // if you want android to have a tutorial video then don't include this...
    document.getElementById("install-button").style.marginTop = "0px";

    if (shouldUpdateApp === true) setupForAppUpdate();
  }

  // ANCHOR: Test Flight
  if (
    (device == "ios" && testFlightLink && !homebase) ||
    (serveTestFlight && device === "ios")
  ) {
    // document.getElementById("ios-abm-howto").style.display = "none";
    // document.getElementById("ios-howto-header").style.display = "none";
    document.getElementById("ios-testflight-howto").style.display = "block";
    if (shouldUpdateApp)
      document.getElementById("install-button").style.marginTop = "0px";
    hideAllBlocks();
    if (shouldUpdateApp === true) {
      // ANCHOR: iPhone Update via Test Flight
      // update via test flight
      // document.getElementById("download-instructions").style.display = "none";
      document.getElementById("ios-testflight-howto").style.display = "none";
      document.getElementById("download-app-text").innerHTML = "Update App";
    } else {
      // ANCHOR: iPhone Download via Test Flight
      // document.getElementById("download-instructions").innerHTML =
      //   '1. Tap "Download" below.<br>2. Download Test Flight<br>3. Tap "Download" one more time!';
      // document.getElementById("ios-testflight-howto")
      // document.getElementById("download-instructions").style.lineHeight = 1.72;
      // document.getElementById("download-instructions").style.marginTop = "13vh";
      document.getElementById("ios-howto-header").style.display = "block";
    }
    document.getElementById("install-block").style.display = "block";
  } else if (device == "ios" && appStoreLink) {
    // ANCHOR: AppStore download (@deprecated)
    // AppStore link (Xtreme Nation)
    hideAllBlocks();
    window.location = appStoreLink;
  } else if (device === "ios") {
    // alert("regular ios")
    document.getElementById("ios-abm-howto").style.display = "block";
    document.getElementById("ios-howto-header").style.display = "block";
    document.getElementById("generate-button-inner").style.display = "block";
  }

  if (device !== "ios" && !debug) {
    // any class of 'ios-only' will  only show up on iOS devices
    var iosElements = document.getElementsByClassName("ios-only");
    Array.prototype.forEach.call(iosElements, function (e) {
      e.style.display = "none";
    });
  }

  if (device === "ios" && shouldUpdateApp === true && !serveTestFlight)
    setupForAppUpdate();

  // showWithCode("ABCXYZ123JKFLZ")
}

function loading() {
  // shows loading gif
  hideAllBlocks();
  document.getElementById("loading-block").style.display = "block";
}

function openLink() {
  // < install app button press
  if (device == "android") {
    if (googlePlaystoreLink) window.open(googlePlaystoreLink, "_blank");
    else
      alert(
        "Please search the Playstore for your team name, or ask a team leader for the link."
      );
    return;
  }

  if ((device == "ios" && testFlightLink && !homebase) || serveTestFlight) {
    window.open(testFlightLink, "_blank");
    return;
  }

  if (!code) return;
  // var link = "https://apps.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" + code;
  // var link =
  //   "itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" +
  //   code;

  // # NOTE: Fix for iOS 15.2
  // var link =
  //   iosVersion >= 15.2
  //     ? "https://apps.apple.com/WebObjects/MZFinance.woa/wa/redeemLandingPage?code=" +
  //       code
  //     : "itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" +
  //       code;
  // # NOTE: But then they reverted back, temporarily?
  var link =
    "itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" +
    code;
  window.open(link, "_blank");
}

function showWithCode(code) {
  hideAllBlocks();
  document.getElementById("install-block").style.display = "block";
  document.getElementById("help-button").style.display = "none";
  document.getElementById("redemption-code").style.display = "block";
  document.getElementById("redemption-code").innerHTML = code;
  if (shouldUpdateApp === true) setupForAppUpdate();
}

function generateCode() {
  // < get code button press
  if (device == "other" && !debug) {
    wrongDevice();
    return;
  }

  if (device == "android") return;

  if (code) {
    openLink();
    return;
  }

  if (fetchAttempted && !fetchFailed) return;
  fetchAttempted = true;

  // show loading gif
  loading();

  // debug
  // showWithCode("XYZ123ABC")
  // return;

  fetch(
    `https://us-central1-primerica-admin.cloudfunctions.net/getCode?team=${team}`
  )
    .then((response) => response.json())
    .then((json) => {
      fetchFailed = false;
      // FYI json.error is untested and may mean nothing
      if (!json || json.error) {
        failed();
        return;
      }
      if (!json.code) {
        if (testFlightLink && testFlightLink.length > 0) {
          alert(
            "No redemption code for your install was found. We are going to serve you an alternative solution, just follow the steps carefully."
          );
          if (device === "ios") serveTestFlight = true;
          onLoad();
        } else {
          alert(
            "No redemption code for your install was found. Please notify an admin."
          );
        }
      } else {
        code = json.code;
        showWithCode(code);
      }
    })
    .catch((error) => {
      if (debug) {
        showWithCode("TESTCODE");
        return;
      }
      console.log("fetch failed with error: " + error);
      fetchFailed = true;
      hideAllBlocks();
      document.getElementById("generate-block").style.display = "block";
      setTimeout(function () {
        if (testFlightLink && testFlightLink.length > 0) {
          alert(
            "Something went wrong. We are going to serve you an alternative solution, just follow the steps carefully."
          );
          if (device === "ios") serveTestFlight = true;
          onLoad();
        } else {
          alert("Something went wrong. Please contact app support.");
        }
      }, 25);
    });
}

function deviceType() {
  // returns either 'ios' or 'android' or 'other'
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  if (/android/i.test(userAgent)) return "android";
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) return "ios";
  if (
    navigator.maxTouchPoints && // iPad Pro
    navigator.maxTouchPoints > 2 &&
    /MacIntel/.test(navigator.platform)
  ) {
    return "ios";
  }
  return "other";
}
function getCountry() {
  return new Promise((resolve, reject) => {
    // fetch("https://extreme-ip-lookup.com/json/")
    fetch("https://ipinfo.io/json", { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((response) => {
        console.log("retrieved country: " + response.country);
        resolve(response.country);
      })
      .catch((data, status) => {
        console.log("Request for country failed. " + JSON.stringify(data));
        resolve("US");
        // return "US";
      });
  });
}
function getURLVaraible(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return false;
}

function setupForAppUpdate() {
  // var e1 = document.getElementById("download-instructions");
  var e2 = document.getElementById("download-app-text");
  var e3 = document.getElementById("ios-abm-howto");
  var e4 = document.getElementById("ios-abm-update-howto");
  var e5 = document.getElementById("ios-howto-header");
  var e6 = document.getElementById("generate-button");
  var e7 = document.getElementById("install-button");
  // if (e1) e1.innerHTML = 'Now just tap "Update App"';
  if (e2) e2.innerHTML = "Update App";
  if (e3 && e3.style) e3.style.display = "none";
  if (e4 && e4.style && iosVersion >= 15.21 && !serveTestFlight)
    e4.style.display = "block";
  if (iosVersion < 15.21 && !serveTestFlight) {
    if (e5 && e5.style) e5.style.display = "none";
    if (e6 && e6.style) e6.style.marginTop = 0;
  } else {
    if (e5) e5.innerHTML = "&#8681; How to update &#8681;";
    if (e6) e6.style.display = "none";
  }
  if (serveTestFlight) {
    if (e4) e4.style.display = "none";
  }
  // if (serveTestFlight) {
  //   if (e7 && e7.style) {// no work
  //     e7.style.marginTop = 0;
  //   }
  // }
}

function goToHelpScreen() {
  window.open("https://support.apptakeoff.com/", "_blank");
}
function wrongDevice() {
  hideAllBlocks();
  setTimeout(function () {
    alert("You must be on a mobile device to download this app.");
  }, 70);
}
function failed() {
  document.getElementById("code").innerHTML = "No redemption code found.";
  alert(
    "Sorry, we were unable to find you a redemption code. Please talk to your team leader."
  );
}

function getIOSVersion() {
  if (navigator.userAgent.match(/ipad|iphone|ipod/i)) {
    //if the current device is an iDevice
    var ios_info = {};
    ios_info.User_Agent = navigator.userAgent;
    ios_info.As_Reported = navigator.userAgent.match(/OS (\d)?\d_\d(_\d)?/i)[0];
    ios_info.Major_Release = navigator.userAgent
      .match(/OS (\d)?\d_\d(_\d)?/i)[0]
      .split("_")[0];
    ios_info.Full_Release = navigator.userAgent
      .match(/OS (\d)?\d_\d(_\d)?/i)[0]
      .replace(/_/g, ".");
    ios_info.Major_Release_Numeric = +navigator.userAgent
      .match(/OS (\d)?\d_\d(_\d)?/i)[0]
      .split("_")[0]
      .replace("OS ", "");
    ios_info.Full_Release_Numeric = +navigator.userAgent
      .match(/OS (\d)?\d_\d(_\d)?/i)[0]
      .replace("_", ".")
      .replace("_", "")
      .replace("OS ", ""); //converts versions like 4.3.3 to numeric value 4.33 for ease of numeric comparisons
    return ios_info;
  }
}
