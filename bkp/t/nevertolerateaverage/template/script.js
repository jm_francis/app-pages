// team, googlePlaystoreLink in unique.js
// var debug = team === "TEAM_NAME"
var debug = false;
var code;
var fetchAttempted;
var fetchFailed;

var update = getURLVaraible("update") == 1 ? true : false;
if (update === true) updateSetup();
console.log("update: " + update);

var device = deviceType(); // "ios", "android", "other"
// device = "ios"; // debug
console.log("device type = " + device);

var country = "United States";
getCountry().then((_country) => {
  country = _country;
  if (homebase && country !== homebase && device === "ios") {
    if (getURLVaraible("admin") == "true") {
      console.log("TestFlight serve cancelled due to admin.");
      return;
    }
    console.log(
      `iPhone user is from ${country} but homebase is ${homebase}... Serving TestFlight link...`
    );
    if (testFlightLink) {
      onLoad({
        serveTestFlight: true,
      });
    } else {
      console.log(
        "Attempted to serve TestFlight to iPhone user, but it is not set in the unique.js"
      );
    }
  }
});

function hideAllBlocks() {
  document.getElementById("generate-block").style.display = "none";
  document.getElementById("install-block").style.display = "none";
  document.getElementById("loading-block").style.display = "none";
}

function onLoad(options = {}) {
  const { serveTestFlight } = options;
  // when page loads
  if (device == "other" && !debug) {
    wrongDevice();
  } else if (device == "android") {
    hideAllBlocks();
    document.getElementById("install-block").style.display = "block";
    document.getElementById("app-img-install").src =
      "../../assets/playstore.svg";
    if (update === true) updateSetup();
  }

  // test flight
  if ((device == "ios" && testFlightLink) || serveTestFlight) {
    hideAllBlocks();
    if (update === true) {
      // update via test flight
      document.getElementById("download-instructions").style.display = "none";
      document.getElementById("download-app-text").innerHTML = "Update App";
    } else {
      // download via test flight
      document.getElementById("download-instructions").innerHTML =
        '1. Tap "Download" below.<br>2. Download Test Flight<br>3. Tap "Download" one more time!';
      document.getElementById("download-instructions").style.lineHeight = 1.72;
      document.getElementById("download-instructions").style.marginTop = "13vh";
    }
    document.getElementById("install-block").style.display = "block";
  } else if (device == "ios" && appStoreLink) {
    // AppStore link (Xtreme Nation)
    hideAllBlocks();
    window.location = appStoreLink;
  }

  if (device !== "ios" && !debug) {
    // any class of 'ios-only' will  only show up on iOS devices
    var iosElements = document.getElementsByClassName("ios-only");
    Array.prototype.forEach.call(iosElements, function (e) {
      e.style.display = "none";
    });
  }

  // showWithCode("ABCXYZ123JKFLZ")
}

function loading() {
  // shows loading gif
  hideAllBlocks();
  document.getElementById("loading-block").style.display = "block";
}

function openLink() {
  // < install app button press
  if (device == "android") {
    if (googlePlaystoreLink) window.open(googlePlaystoreLink, "_blank");
    else
      alert(
        "Please search the Playstore for your team name, or ask a team leader for the link."
      );
    return;
  }

  if (device == "ios" && testFlightLink) {
    window.open(testFlightLink, "_blank");
    return;
  }

  if (!code) return;
  // var link = "https://apps.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" + code;
  var link =
    "itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/freeProductCodeWizard?code=" +
    code;
  window.open(link, "_blank");
}

function showWithCode(code) {
  hideAllBlocks();
  document.getElementById("install-block").style.display = "block";
  document.getElementById("help-button").style.display = "none";
  document.getElementById("redemption-code").style.display = "block";
  document.getElementById("redemption-code").innerHTML = code;
  if (update === true) updateSetup();
}

function generateCode() {
  // < get code button press
  if (device == "other" && !debug) {
    wrongDevice();
    return;
  }

  if (device == "android") return;

  if (code) {
    openLink();
    return;
  }

  if (fetchAttempted && !fetchFailed) return;
  fetchAttempted = true;

  // show loading gif
  loading();

  // debug
  // showWithCode("XYZ123ABC")
  // return;

  fetch(
    `https://us-central1-primerica-admin.cloudfunctions.net/getCode?team=${team}`
  )
    .then((response) => response.json())
    .then((json) => {
      fetchFailed = false;
      if (!json || !json.code) {
        failed();
        return;
      }
      code = json.code;
      showWithCode(code);
    })
    .catch((error) => {
      if (debug) {
        showWithCode("TESTCODE");
        return;
      }
      fetchFailed = true;
      hideAllBlocks();
      document.getElementById("generate-block").style.display = "block";
      setTimeout(function () {
        alert("Something went wrong. Please contact app support.");
      }, 25);
    });
}

function deviceType() {
  // returns either 'ios' or 'android' or 'other'
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  if (/android/i.test(userAgent)) return "android";
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) return "ios";
  if (
    navigator.maxTouchPoints && // iPad Pro
    navigator.maxTouchPoints > 2 &&
    /MacIntel/.test(navigator.platform)
  ) {
    return "ios";
  }
  return "other";
}
function getCountry() {
  return new Promise((resolve, reject) => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        resolve(response.country);
        // return response.country;
      })
      .catch((data, status) => {
        console.log("Request for country failed.");
        resolve("United States");
        // return "United States";
      });
  });
}
function getURLVaraible(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return false;
}

function updateSetup() {
  var e1 = document.getElementById("download-instructions");
  var e2 = document.getElementById("download-app-text");
  if (e1) e1.innerHTML = 'Now just tap "Update App"';
  if (e2) e2.innerHTML = "Update App";
}

function goToHelpScreen() {
  window.open("https://chat.apptakeoff.com/", "_blank");
}
function wrongDevice() {
  hideAllBlocks();
  setTimeout(function () {
    alert("You must be on a mobile device to download this app.");
  }, 70);
}
function failed() {
  document.getElementById("code").innerHTML = "No redemption code found.";
  alert(
    "Sorry, we were unable to find you a redemption code. Please talk to your team leader."
  );
}
