#!/bin/bash

cd t

for d in *; do
    if [[ $(cat "$d/unique.js") == *"// var appStoreLink"* ]]; then
        sed -i '' "s/\/\/ var appStoreLink/var appStoreLink/g" "$d/unique.js"
        echo "Added in for $d"
    fi

    # if [[ $(cat "$d/unique.js") == *"var appStoreLink"* ]]; then
    #     echo "*"
    # else
    #     echo "var appStoreLink = null;" >> "$d/unique.js"
    #     echo "Added in for $d"
    # fi
done
