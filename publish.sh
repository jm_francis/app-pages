#!/bin/bash

if [ -z "$1" ]; then

echo "Use the command like this: ./publish \"<commit message>\""

else

git add .
git commit -m "$1"

git pull origin master
git push origin master

echo $'\n\nPublished!'

fi
