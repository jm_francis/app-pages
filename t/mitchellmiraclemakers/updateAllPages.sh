#!/bin/bash

cd ..

# ANCHOR: Backup
echo "Backing up teams..."
cd ..
mkdir bkp
cp -r t bkp/
cd t

echo "Updating teams..."
for d in */ ; do
    name=${d//\/}
    echo "Updating $name"
    if [ "$name" = "template" ]; then
        continue
    fi
    cp template/script.js "$name/"
    cp template/index.html "$name/"
done

echo "DONE!"
