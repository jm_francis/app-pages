/**
 * This should be the exact name of the collection in the Firestore
 */
var team = "RisingStars";

/**
 * Sends android users to given link
 */
var googlePlaystoreLink = "https://play.google.com/store/apps/details?id=com.apptakeoff.risingstars";

/**
 * If testFlightLink is set but no homebase is set, testFlightLink will be served for EVERYONE!
 * If testFlightLink is set and a homebase is set, the testFlightLink will only be served if the user is not in the same country as "homebase"
 */
var testFlightLink = null;

/**
 * Setting homebase to the main country of the app will cause other countries to pull the testFlightLink instead, if it is provided.
 */
var homebase = "US";

/**
 * Only in use for Xtreme Nation, and just instantly redirects user to the provided link if it is provided
 * This is intended for use for in app prompted updates (get-my.app/t/xtremenation/?update=1)
 */
var appStoreLink = "https://apps.apple.com/app/rising-stars-hierarchy/id6639619628";
// var appStoreLink = "https://apps.apple.com/us/app/xtreme-nation/id1360107458";

//debug
// var testFlightLink = "https://apptakeoff.com/";
// var homebase = "US";
